from django.shortcuts import render, redirect
from django.contrib import messages
from django.shortcuts import get_object_or_404, get_list_or_404
from .models import *



# Create your views here.

def index(request):
    program = get_list_or_404(Program.objects.all())
    banners = Banner.objects.all()
    blogs = get_list_or_404(Blog.objects.all())

    context = {
        'programs':program,
        'banners':banners,
        'blogs':blogs
    }

    return render(request, 'page/index.html',context)   

def contact(request):
    context = {
        'is_contact':True
    }
    return render(request, 'page/kontak.html',context)

def about(request):
    context = {
        'is_about':True
    }
    return render(request, 'page/tentang.html',context)

def blog(request):
    blogs = get_list_or_404(Blog.objects.all())
    program = get_list_or_404(Program.objects.all())


    context={
        'blogs':blogs,
        'is_blog':True,
        'program':program,
    }
    return render(request, 'page/blog.html', context)

def destinations(request):
    return render(request, 'page/destinations.html')



def blog_detail(request, slug):
    blog = get_object_or_404(Blog,slug=slug)
    other_blog = Blog.objects.exclude(slug=slug)
    program = get_list_or_404(Program.objects.all())

    context={
        'blog':blog,
        'is_detail_blog':True,
        'program':program,
        'other_blog':other_blog
    }
    return render(request, 'page/blog_detail.html', context)

def detail_paket(request, slug):

    program = Program.objects.get(slug=slug)
    pakets = program.program_package.all()  

    other = Program.objects.exclude(slug=slug)



    context={
        'program':program,
        'pakets':pakets,
        'other':other,
        'category':program.category,
        'is_detail':True
    }

    if request.method == 'POST':
        name = request.POST.get('name')
        address = request.POST.get('address')
        phone_number = request.POST.get('phone')
        jumlah_peserta = request.POST.get('jumlah')
        tanggal_jemput = request.POST.get('tanggal_jemput')
        package_name = request.POST.get('pkt')

         # Dapatkan instance Package berdasarkan nama paket
        package = get_object_or_404(Package, name=package_name)


        Booking.objects.create(
            name=name,
            address=address,
            phone_number=phone_number,
            jumlah_peserta=jumlah_peserta,
            tanggal_jemput=tanggal_jemput,
            package=package
        )

        Booking.save
        messages.success(request,'PESANAN SUKSES')
        return redirect('/')


    return render(request, 'page/detail.html', context)


