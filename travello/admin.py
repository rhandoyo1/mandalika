from django.contrib import admin
from .models import *

# Register your models here.

class BookingAdmin(admin.ModelAdmin):
    readonly_fields = ['tanggal_jemput','name','address','phone_number','jumlah_peserta','package']

admin.site.register(Program)
admin.site.register(Package)
admin.site.register(Destination)
admin.site.register(Testimonial)
admin.site.register(Banner)
admin.site.register(Gallery)
admin.site.register(Booking, BookingAdmin)
admin.site.register(Blog)
admin.site.register(Aktivitas)
admin.site.register(Hari)
admin.site.register(PackageDay)
admin.site.register(ExcludeList)
admin.site.register(ListInclude)
admin.site.register(Include)


