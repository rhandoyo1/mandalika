from django.db import models

# Create your models here.

from django.db import models
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
import sys
from PIL import Image

from distutils.command.upload import upload
from email.policy import default
from statistics import mode
from django.urls import reverse

from django.utils.text import slugify


CATEGORY_CHOICES = (
    ('Paket Honeymoon','Paket Honeymoon'),
    ('Paket Tour','Paket Tour'),
    ('Paket Harian','Paket Harian'),

)

class Program(models.Model):
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=200, choices=CATEGORY_CHOICES, default='')
    price = models.CharField(max_length=100)
    image = models.ImageField(upload_to='parentPackage')
    slug = models.SlugField(blank=True, null=True, editable=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Destination(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ExcludeList(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return f"Hari {self.name}"

class Package(models.Model):
    name = models.CharField(max_length=100)
    program = models.ForeignKey(Program, related_name='program_package', default="", on_delete=models.CASCADE)
    durasi = models.CharField(max_length=100, default='')
    destinasi = models.ManyToManyField(Destination, default='', related_name='destinations')
    deskripsi = models.TextField(default='',  blank=True, null=True)
    price = models.CharField(max_length=100)
    image = models.ImageField(upload_to='imgPackage')
    image_price = models.ImageField(upload_to='imgPrice', default='',  blank=True, null=True)
    # itenerary = RichTextUploadingField()
    exclude = models.ManyToManyField(ExcludeList, related_name='excludeLiat')

    def __str__(self):
        return self.name
    
class ListInclude(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    
class Include(models.Model):
    package = models.ForeignKey(Package, related_name='package_include', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    aktivitas = models.ManyToManyField(ListInclude, related_name='include_list')

    def __str__(self):
        return f"Hari {self.name}"
    

    


    
class Aktivitas(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Hari(models.Model):
    kode = models.CharField(max_length=20, unique=True, default='')
    name = models.CharField(max_length=200)
    aktivitas = models.ManyToManyField(Aktivitas, related_name='hari_aktivitas')

    def __str__(self):
        return f"Hari {self.kode}"
    

class PackageDay(models.Model):
    package = models.ForeignKey(Package, related_name='package_days', on_delete=models.CASCADE)
    hari = models.ForeignKey(Hari, related_name='package_days', on_delete=models.CASCADE)
    urutan = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.package.name} - {self.hari.name} (Hari {self.urutan})"


# price


class Testimonial(models.Model):
    author = models.CharField(max_length=100)
    rating = models.FloatField()
    text = models.TextField()
    date = models.DateField()
    photo_url = models.URLField(blank=True, null=True)  # Field untuk URL foto

    def __str__(self):
        return self.author
    

class Gallery(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='gallery', default='')
    desc = models.TextField()

    def __str__(self):
        return self.name


class Banner(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to="banner", default='')

    def __str__(self):
        return self.title


class Booking(models.Model):
    STATUS_CHOICES = [
        ('PEND', 'Pending'),
        ('CONF', 'Confirmed'),
        ('CANC', 'Cancelled'),
        ('COMP', 'Completed'),
    ]

    name = models.CharField(max_length=200, editable=False)
    address = models.CharField(max_length=200, editable=False)
    phone_number = models.CharField(max_length=200, editable=False)
    jumlah_peserta = models.CharField(max_length=100, editable=False)
    tanggal_jemput = models.DateField(editable=False)
    package = models.ForeignKey('Package', on_delete=models.CASCADE, editable=False)
    status = models.CharField(max_length=4, choices=STATUS_CHOICES, default='PEND')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Blog(models.Model):
    title = models.CharField(max_length=200)
    deskripsi = models.TextField(default='')
    image = models.ImageField(upload_to='blog_image', null=True, blank=True)
    content = RichTextUploadingField()
    tanggal = models.DateField(auto_now_add=True)
    penulis = models.CharField(max_length=100)
    slug = models.SlugField(blank=True, null=True, editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

