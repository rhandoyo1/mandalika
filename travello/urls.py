from  django.urls import path
from . import views

app_name='_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('kontak', views.contact, name='kontak'),
    path('tentang', views.about, name='tentang'),
    path('blog', views.blog, name='blog'),
    path('blog-detail/<str:slug>', views.blog_detail, name='blog_detail'),
    path('destinations', views.destinations, name='destinations'),
    path('detail-paket/<str:slug>', views.detail_paket, name='detail_paket')
]